package cz.vse.weatherapp.domain.jpa.repository;

import cz.vse.weatherapp.domain.jpa.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<AppUser, Long> {
    AppUser findByUserName(String userName); // find a user by their userName

}